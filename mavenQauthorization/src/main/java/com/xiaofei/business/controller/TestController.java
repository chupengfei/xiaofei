package com.xiaofei.business.controller;

import com.alibaba.fastjson.JSONObject;
import com.sun.jnlp.ApiDialog;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class TestController {

    /**
     *
     * {
     *     "name": "褚鹏飞",
     *     "birth": "2019-11-16 22:14:58", //消息转换器
     *     "addr": null, // 消息转换器
     *     "age": 11
     * }
     * CommonConfig fastJsonConfigure
     *
     * @Auther: chupengfei_dxm
     * @Date: 2019/11/16 22:15
     */
    @PostMapping("testJson")
    public Object testJson(){
        JSONObject object = new JSONObject();
        object.put("name","mavenQ");
        object.put("age",11);
        object.put("birth",new Date());
        object.put("address",null);
        return object;
    }
}
